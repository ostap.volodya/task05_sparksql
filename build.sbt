name := "task05_sparksql"

version := "0.1"

libraryDependencies ++= Seq(
  "org.apache.spark" % "spark-core_2.12" % "2.4.4",
  "org.apache.spark" % "spark-sql_2.12" % "2.4.4",
  "org.apache.maven.plugins" % "maven-shade-plugin" % "3.2.1"
)