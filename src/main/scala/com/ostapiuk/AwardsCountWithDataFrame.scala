package com.ostapiuk

import org.apache.spark.sql.{Dataset, Row, SparkSession}

object AwardsCountWithDataFrame {
  def main(args: Array[String]): Unit = {
    val spark = SparkSession
      .builder()
      .master("local")
      .appName("PlayerAwardsCount")
      .config("spark.some.config.option", "some-value")
      .getOrCreate()

    createDataFrame(spark, "src/main/resources/AwardsPlayers.csv")
      .createOrReplaceTempView("fullAwards")
    createDataFrame(spark, "src/main/resources/Master.csv")
      .createOrReplaceTempView("fullPlayers")

    joinDataFrames(spark)
    countAwards(spark)
    printResults(spark)
    spark.stop()
  }

  /*
   * Creates a DataFrame based on the content of a file
   */
  private def createDataFrame(session: SparkSession,
                              path: String): Dataset[Row] = {
    session.read.csv(path)
  }

  /*
   * Enables application to run SQL queries programmatically and returns the result as a Dataset<Row>
   */
  private def selectFields(session: SparkSession,
                           fields: String,
                           table: String): Dataset[Row] = {
    session.sql("SELECT " + fields + " FROM " + table)
  }

  /*
   * Creates a dataframe of pairs with all pairs of elements for each id
   */
  private def joinDataFrames(session: SparkSession): Unit = {
    selectFields(session, "_c0 as playerID, _c1 as award", "fullAwards")
      .createOrReplaceTempView("awards")
    selectFields(session,
      "_c0 as playerID, _c3 as firstName, _c4 as lastName", "fullPlayers")
      .createOrReplaceTempView("players")
    selectFields(session, "players.playerID, firstName, lastName, award",
      "players INNER JOIN awards ON players.playerID = awards.playerID")
      .createOrReplaceTempView("joined")
  }

  /*
   * Counts the number of rows that matches a specified criteria
   */

  private def countAwards(sparkSession: SparkSession): Unit = {
    selectFields(sparkSession, "playerID, COUNT(award) as awardsAmount, firstName, lastName",
      "joined GROUP BY playerID, firstName, lastName")
      .createOrReplaceTempView("counted")
  }

  /*
   * Prints result of SQL queries to the file
   */
  private def printResults(session: SparkSession): Unit = {
    val printedDF = session.sql("SELECT CONCAT('Player - ', firstName, ' ', lastName, "
      + "', his id: ', playerID, ', number of awards -> ', CAST(awardsAmount AS STRING)) as result FROM counted")
    printedDF
      .coalesce(1)
      .write
      .option("header", "false")
      .option("delimiter", "\t")
      .csv("src/main/resources/outputDFcvs")
    printedDF
      .coalesce(1)
      .write
      .option("header", "false")
      .text("src/main/resources/outputDFtxt")
  }
}
